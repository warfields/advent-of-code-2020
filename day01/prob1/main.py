#!/usr/bin/python3

import re

reg = re.compile("(\d+)-(\d+) ([a-z])+: ([a-z]*)")

with open('input.txt', 'r') as f:
    data = [reg.match(line).groups() for line in f]
    data = [(int(x[0])-1,int(x[1])-1,x[2], x[3]) for x in data]

good = 0

for passwd in data:
    # ^ is an XOR opperator
    if (passwd[-1][passwd[0]] is passwd[2]) ^ (passwd[-1][passwd[1]] is passwd[2]):
        good += 1

print(good)