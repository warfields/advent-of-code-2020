#!/usr/bin/python3

import re

reg = re.compile("(\d+)-(\d+) ([a-z])+: ([a-z]*)")

with open('input.txt', 'r') as f:
    data = [reg.match(line).groups() for line in f]
    data = [(int(x[0]),int(x[1]),x[2], x[3]) for x in data]

good = 0

for passwd in data:
    occ = passwd[-1].count(passwd[2]) 
    if occ <= passwd[1] and occ >= passwd[0]:
        good += 1

print(good)