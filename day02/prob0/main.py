#!/usr/bin/python3

with open('input.txt', 'r') as f:
    data = [line for line in f]

col = 0
trees = 0

for row in range(len(data)):
    if data[row][col] is '#':
        trees += 1
    col += 3
    col = col % len(data[-1])

print(trees)