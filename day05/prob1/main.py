#!/usr/bin/python3

groups = []

with open('input.txt', 'r') as f:
    cur = []
    for line in f:
        if line[:-1] == "":
            groups.append(cur)
            cur = []
            continue
        cur.append(line[:-1])

answers = 0

for group in groups:
    first = True
    combo = ""
    for person in group:
        if first:
            combo = person
            first = False
            continue

        letters = ""
        for letter in combo:
            if letter in person:
                letters += letter
        
        combo = letters
        
    answers += len(combo)

print(answers)
