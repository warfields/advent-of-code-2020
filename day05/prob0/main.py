#!/usr/bin/python3

groups = []

with open('input.txt', 'r') as f:
    cur = []
    for line in f:
        if line[:-1] == "":
            groups.append(cur)
            cur = []
            continue
        cur.append(line[:-1])

answers = 0

for jeff in groups:
    combo = ""
    for john in jeff:
        combo += john
    


    letters = ""
    for letter in combo:
        if letter not in letters:
            letters += letter
    
    answers += len(letters)

print(answers)